package com.example.imc;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText editAltura, editPeso;
    private TextView lblVerIMC;
    private Button calcular, limpiar, cerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editAltura= (EditText) findViewById(R.id.txtAltura);
        editPeso= (EditText) findViewById(R.id.txtPeso);
        lblVerIMC= (TextView) findViewById(R.id.viewIMC);
        calcular= (Button) findViewById(R.id.btnCalcular);
        limpiar= (Button) findViewById(R.id.btnClean);
        cerrar= (Button) findViewById(R.id.btnClose);


        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String pesoText = editPeso.getText().toString();
                String alturaText = editAltura.getText().toString();

                if (pesoText.isEmpty() || alturaText.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Capture el texto", Toast.LENGTH_SHORT).show();
                }

                else{
                    float imc= 0.0f, estatura =0.0f, peso=0.0f;
                    estatura = Float.parseFloat(editAltura.getText().toString());
                    peso = Float.parseFloat(editPeso.getText().toString());
                    imc = peso/(estatura*estatura);
                    lblVerIMC.setText("IMC:    " + String.valueOf(imc) + "    kg/m2");
                }

            }
        });

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               editAltura.setText("");
               editPeso.setText("");
               lblVerIMC.setText("");


            }
        });


        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder= new AlertDialog.Builder( MainActivity.this);
                builder.setTitle("IMC");
                builder.setMessage("esta seguro?");





                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                });


                builder.setNegativeButton("no", null);
                builder.show();




            }
        });





    }
}